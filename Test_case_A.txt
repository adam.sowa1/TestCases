Feature: Find "Prowly Media Monitoring" using bing.com

Scenario: Check if "Prowly Media Monitoring" will be correctly found by the Bing.com search engine
    Given User opens bing.com website
    When Open Bing.com homepage
    Then Provide "Prowly Media Monitoring" in the search bar
    When Click on the first displayed record
    Then User is redirected to the search results page with the correct results

Scenario: Check if "Prowly Media Monitoring" will be correctly filtered by the "Video" category
    Given User opens bing.com website
    When Open Bing.com homepage
    Then Provide "Prowly Media Monitoring" in the search bar
    When Click on the first displayed record
    Then User is redirected to the search results page with the correct results
    When Click the "Video" category on the top category bar
    Then Search results include only videos related to "Prowly Media Monitoring"

Scenario: Check if results are filtered correctly by different filters
    Given User opens bing.com website
    When Open Bing.com homepage
    Then Provide "Prowly Media Monitoring" in the search bar
    When Click on the first displayed record
    Then User is redirected to the search results page with the correct results
    When Click the "Video" category on the top category bar
    Then Search results include only videos related to "Prowly Media Monitoring"
    When Click the "Images" category on the top category bar
    Then Search results include only images related to "Prowly Media Monitoring"

//In my current company I used only "When" and "Then" in the test cases. So I used others below for this same test cases

Feature: Find "Prowly Media Monitoring" using bing.com

Scenario: Check if "Prowly Media Monitoring" will be correctly found by the Bing.com search engine
    Given User opens bing.com website
    When Open Bing.com homepage
    Then Provide "Prowly Media Monitoring" in the search bar
    And Click on the first displayed record
    Then User is redirected to the search results page with the correct results

Scenario: Check if "Prowly Media Monitoring" will be correctly filtered by the "Video" category
    Given User opens bing.com website
    When Open Bing.com homepage
    Then Provide "Prowly Media Monitoring" in the search bar
    And Click on the first displayed record
    Then User is redirected to the search results page with the correct results
    And Click the "Video" category on the top category bar
    Then Search results include only videos related to "Prowly Media Monitoring"

Scenario: Check if results are filtered correctly by different filters
    Given User opens bing.com website
    When Open Bing.com homepage
    Then Provide "Prowly Media Monitoring" in the search bar
    And Click on the first displayed record
    Then User is redirected to the search results page with the correct results
    And Click the "Video" category on the top category bar
    Then Search results include only videos related to "Prowly Media Monitoring"
    And Click the "Images" category on the top category bar
    Then Search results include only images related to "Prowly Media Monitoring"

